package com.backend_service.ecommerce;

import com.backend_service.ecommerce.Dto.AllCategoryDTO;
import com.backend_service.ecommerce.Repository.ChildCategoryRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@SpringBootTest
class ECommerceApplicationTests {
    @Autowired
    ChildCategoryRepository childCategoryRepository;

    @Test
    void contextLoads() {
    }

    //    @Test
//    public void getAllCategories(){
//        List<CategoryResponseModel> categories=childCategoryRepository.findByNameNative("Top wear");
//        System.out.println(categories);
//    }
    @Test
    public void getAllCategories() {
        List<AllCategoryDTO> allCategories = childCategoryRepository.getAllCategories();
        Map<Long, List<AllCategoryDTO>> collect = allCategories.stream().collect(Collectors.groupingBy(all -> all.getMainCategory().getMainCategoryId()));
        System.out.println(collect.size());
        int i = 0;
        for (Map.Entry<Long, List<AllCategoryDTO>> entry : collect.entrySet()) {
            System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue().toString());
        }
    }
}
