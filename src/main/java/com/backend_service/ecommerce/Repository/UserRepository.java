package com.backend_service.ecommerce.Repository;

import com.backend_service.ecommerce.Entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<UserEntity,Long> {
    @Query("SELECT ui from User_info ui where ui.email=?1")
    Optional<UserEntity> findByEmail(String email);
}
