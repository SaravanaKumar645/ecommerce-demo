package com.backend_service.ecommerce.Repository;

import com.backend_service.ecommerce.Entity.SubCategoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SubCategoryRepository extends JpaRepository<SubCategoryEntity,Long> {
    @Query("select sc from SubCategory sc where sc.mainCategory.id=?1 ")
    List<SubCategoryEntity> findByMainCategoryId(Long id);

    @Query("select sc from SubCategory sc where upper(sc.subCategoryName) =upper(?1) ")
    Optional<SubCategoryEntity> findByNameIgnoreCase(String name);
}
