package com.backend_service.ecommerce.Repository;

import com.backend_service.ecommerce.Entity.ChildCategoryEntity;
import com.backend_service.ecommerce.Entity.ProductEntity;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface ProductRepository extends JpaRepository<ProductEntity,Long> {
    @Query("select pl from productList pl where pl.childCategory.id=?1")
    List<ProductEntity> findByChildCategoryId(Long id);
    @Query("Select p from productList p where upper(p.productName)=upper(?1)")
    Optional<ProductEntity> findByName(String productName);
}
