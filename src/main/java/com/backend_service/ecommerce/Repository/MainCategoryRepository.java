package com.backend_service.ecommerce.Repository;

import com.backend_service.ecommerce.Entity.MainCategoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MainCategoryRepository extends JpaRepository<MainCategoryEntity,Long> {
    @Query("select distinct mc from MainCategory mc where upper(mc.categoryName) =upper(?1) ")
    Optional<MainCategoryEntity> findByNameIgnoreCase(String name);

//    @Query(
//            value = "select mc.category_id, mc.category_name,sc.sub_category_id,sc.sub_category_name " +
//                    "from main_category mc left join sub_category sc on mc.category_id=sc.category_id",
//            nativeQuery = true
//    )
//    List<MainCategory_Entity> getAllCategories();
}
