package com.backend_service.ecommerce.Repository;

import com.backend_service.ecommerce.Dto.AllCategoryDTO;
import com.backend_service.ecommerce.Entity.ChildCategoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ChildCategoryRepository extends JpaRepository<ChildCategoryEntity,Long> {
    @Query("select cc from ChildCategory cc where upper(cc.name) =upper(?1) ")
    Optional<ChildCategoryEntity> findByName(String name);

    @Query("select cc from ChildCategory cc where cc.subCategory.id=?1 ")
    List<ChildCategoryEntity> findBySubCategoryId(Long id);

    @Query(value = "select cc.child_category_id childCategoryId,cc.child_category_name childCategoryName,\n" +
            "        sc.sub_category_id subCategoryId,sc.sub_category_name subCategoryName,\n" +
            "        mc.category_id mainCategoryId,mc.category_name mainCategoryName\n" +
            "        from main_category mc, sub_category sc, child_category cc\n" +
            "        where mc.category_id=sc.category_id and sc.sub_category_id=cc.sub_category_id order by mc.category_id,sc.sub_category_id;\n",
            nativeQuery = true)
    List<AllCategoryDTO> getAllCategories();

//    @Query(value = "select\n" +
//            "cc.child_category_id childCategoryId,cc.child_category_name childCategoryName,\n"+
//            "sc.sub_category_id subCategoryId,sc.sub_category_name subCategoryName\n" +
//            "from child_category cc,sub_category sc\n" +
//            "where cc.sub_category_id=sc.sub_category_id  order by sc.sub_category_id;\n ",nativeQuery = true)
//    List<AllCategoryDTO> getAllCategories();
}

//Query to fetch all categories
   /* select cc.child_category_id childID,cc.child_category_name childName,
        sc.sub_category_id subID,sc.sub_category_name subName,
        mc.category_id mainID,mc.category_name mainName
        from main_category mc, sub_category sc, child_category cc
        where mc.category_id=sc.category_id and sc.sub_category_id=cc.sub_category_id order by mc.category_id,sc.sub_category_id;

        select cc.child_category_id childID,cc.child_category_name childName,
        sc.sub_category_id subID,sc.sub_category_name subName,
        mc.category_id mainID,mc.category_name mainName
        from((child_category cc
        inner join sub_category sc on cc.sub_category_id = sc.sub_category_id)
        inner join main_category mc on sc.category_id=mc.category_id);
    */


