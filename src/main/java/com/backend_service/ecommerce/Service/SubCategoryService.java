package com.backend_service.ecommerce.Service;

import com.backend_service.ecommerce.Dto.SubCategoryDTO;
import com.backend_service.ecommerce.Entity.SubCategoryEntity;
import com.backend_service.ecommerce.Exceptions.ResourceNotFoundException;
import com.backend_service.ecommerce.Repository.MainCategoryRepository;
import com.backend_service.ecommerce.Repository.SubCategoryRepository;
import com.backend_service.ecommerce.Entity.MainCategoryEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class SubCategoryService {

    private final SubCategoryRepository subCategoryRepository;
    private final MainCategoryRepository mainCategoryRepository;


    @Autowired
    public SubCategoryService(SubCategoryRepository subCategoryRepository, MainCategoryRepository mainCategoryRepository) {
        this.subCategoryRepository = subCategoryRepository;
        this.mainCategoryRepository = mainCategoryRepository;
    }

    // adding sub category with main category id
    public SubCategoryDTO addSubCategory(Long mainCategoryId, SubCategoryDTO subCategoryDTO) {
        Optional<MainCategoryEntity> mainCategory = mainCategoryRepository.findById(mainCategoryId);

        if (mainCategory.isPresent()) {
            SubCategoryEntity subCategoryEntity = modelToEntity(subCategoryDTO);
            subCategoryEntity.setMainCategory(mainCategory.get());
            SubCategoryEntity savedCategory = subCategoryRepository.save(subCategoryEntity);
            return entityToModel(savedCategory);
        } else {
            throw new ResourceNotFoundException("Main category with id :" + mainCategoryId + " not found !");
        }
    }

    // fetching all sub categories
    public List<SubCategoryDTO> getAllSubCategories() {
        List<SubCategoryEntity> subCategories = subCategoryRepository.findAll();
        return subCategories
                .stream()
                .map(this::entityToModel)
                .collect(Collectors.toList());
    }


    // Object Mapping Methods
    // MODEL to ENTITY object mapping
    private SubCategoryEntity modelToEntity(SubCategoryDTO subCategoryDTO) {
        return SubCategoryEntity.builder()
                .subCategoryName(subCategoryDTO.getSubCategoryName())
                .build();
    }

    // ENTITY to MODEL object mapping
    private SubCategoryDTO entityToModel(SubCategoryEntity subCategoryEntity) {
        return SubCategoryDTO.builder()
                .subCategoryId(subCategoryEntity.getId())
                .subCategoryName(subCategoryEntity.getSubCategoryName())
                .mainCategoryID(subCategoryEntity.getMainCategory().getId())
                .build();
    }

}
