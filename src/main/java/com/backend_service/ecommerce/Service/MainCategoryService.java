package com.backend_service.ecommerce.Service;

import com.backend_service.ecommerce.Dto.MainCategoryDTO;
import com.backend_service.ecommerce.Exceptions.DuplicateEntryException;
import com.backend_service.ecommerce.Repository.MainCategoryRepository;
import com.backend_service.ecommerce.Entity.MainCategoryEntity;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class MainCategoryService {

    private final MainCategoryRepository mainCategoryRepository;

    public MainCategoryService(MainCategoryRepository mainCategoryRepository) {
        this.mainCategoryRepository = mainCategoryRepository;
    }


    // adding main category
    public MainCategoryDTO addMainCategory(MainCategoryDTO mainCategoryDTO) {
        Optional<MainCategoryEntity> fetchedCategory = mainCategoryRepository.findByNameIgnoreCase(mainCategoryDTO.getMainCategoryName());
        if (fetchedCategory.isPresent()) {
            throw new DuplicateEntryException(mainCategoryDTO.getMainCategoryName()+" already exists !");
        }
        MainCategoryEntity savedCategory = mainCategoryRepository.save(modelToEntity(mainCategoryDTO));
        return entityToModel(savedCategory);
    }

    // fetching all main categories
    public List<MainCategoryDTO> getAllMainCategories() {
        List<MainCategoryEntity> categories = mainCategoryRepository.findAll();
        return categories
                .stream()
                .map(this::entityToModel)
                .collect(Collectors.toList());
    }

    // Object Mapping Methods
    // MODEL to ENTITY object mapping
    public MainCategoryEntity modelToEntity(MainCategoryDTO mainCategoryDTO) {
        return MainCategoryEntity.builder()
                .categoryName(mainCategoryDTO.getMainCategoryName())
                .build();
    }

    // ENTITY to MODEL object mapping
    public MainCategoryDTO entityToModel(MainCategoryEntity mainCategoryEntity) {
        return MainCategoryDTO.builder()
                .mainCategoryName(mainCategoryEntity.getCategoryName())
                .mainCategoryId(mainCategoryEntity.getId())
                .build();
    }
}
