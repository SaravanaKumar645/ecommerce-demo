package com.backend_service.ecommerce.Service;

import com.backend_service.ecommerce.Dto.UserDTO;
import com.backend_service.ecommerce.Entity.UserEntity;
import com.backend_service.ecommerce.Exceptions.DuplicateEntryException;
import com.backend_service.ecommerce.Exceptions.PasswordNotMatchException;
import com.backend_service.ecommerce.Exceptions.ResourceNotFoundException;
import com.backend_service.ecommerce.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {

    private final UserRepository userRepository;


    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    public UserDTO registerUser(UserDTO userDTO) {
        Optional<UserEntity> user = userRepository.findByEmail(userDTO.getEmail());
        if (user.isPresent()) {
            throw new DuplicateEntryException("User with email :"+ userDTO.getEmail()+" already exists !");
        }
        UserEntity userEntity = userRepository.save(modelToEntity(userDTO));
        return entityToModel(userEntity);
    }

    public UserDTO loginUser(UserDTO userDTO) {
        Optional<UserEntity> user = userRepository.findByEmail(userDTO.getEmail());
        if (user.isPresent()) {
            if (user.get().getPassword().equals(userDTO.getPassword())) {
                return entityToModel(user.get());
            }else {
                throw new PasswordNotMatchException("Check your password and try again !");
            }
        }
       throw new ResourceNotFoundException("User with email :"+ userDTO.getEmail()+" not exists !");
    }


    // Object Mapping Methods
    // MODEL to ENTITY object mapping
    private UserEntity modelToEntity(UserDTO userDTO) {
        return UserEntity.builder()
                .email(userDTO.getEmail())
                .name(userDTO.getName())
                .password(userDTO.getPassword())
                .phone(userDTO.getPhone())
                .build();
    }

    // ENTITY to MODEL object mapping
    private UserDTO entityToModel(UserEntity userEntity) {
        return UserDTO.builder()
                .id(userEntity.getId())
                .email(userEntity.getEmail())
                .name(userEntity.getName())
                .password(null)
                .phone(userEntity.getPhone())
                .build();
    }
}
