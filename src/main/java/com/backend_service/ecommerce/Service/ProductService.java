package com.backend_service.ecommerce.Service;

import com.backend_service.ecommerce.Dto.ProductDTO;
import com.backend_service.ecommerce.Entity.ChildCategoryEntity;
import com.backend_service.ecommerce.Entity.ProductEntity;
import com.backend_service.ecommerce.Exceptions.ResourceNotFoundException;
import com.backend_service.ecommerce.Repository.ChildCategoryRepository;
import com.backend_service.ecommerce.Repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProductService {

    private final ProductRepository product_repository;
    private final ChildCategoryRepository childCategory_repository;

    @Autowired
    public ProductService(ProductRepository product_repository,ChildCategoryRepository childCategory_repository) {
        this.childCategory_repository = childCategory_repository;
        this.product_repository = product_repository;
    }

    // saving products under its child category
    public ProductDTO addProductList(Long id, ProductDTO productDTO) {
        Optional<ChildCategoryEntity> childCategory = childCategory_repository.findById(id);

        if (childCategory.isPresent() ){
            ProductEntity productEntity = modelToEntity(productDTO);
            productEntity.setChildCategory(childCategory.get());
            ProductEntity savedProduct=product_repository.save(productEntity);
            return entityToModel(savedProduct);
        }else {
            throw new ResourceNotFoundException("Child category with id :"+id+" not exists !");
        }
    }

    // fetching product by its id or name
    public ProductDTO getProductByIdOrName(String query, boolean isDigit) {
        Optional<ProductEntity> product = isDigit
                ?product_repository.findById(Long.parseLong(query))
                :product_repository.findByName(query);
        if (product.isPresent()){
            return entityToModel(product.get());
        }
        throw new ResourceNotFoundException("Product with "+query+" not exists !");

    }

    // fetching all product list
    public List<ProductDTO> getAllProduct() {
        List<ProductEntity> productEntities= product_repository.findAll();

        return productEntities
                .stream()
                .map(this::entityToModel)
                .collect(Collectors.toList());
    }

    // fetching all products with respect to its child category
    public List<ProductDTO> getAllProductsByChildCategory(Long childCategoryId){
        List<ProductEntity> productEntityList=product_repository.findByChildCategoryId(childCategoryId);
        productEntityList.forEach(System.out::println);
        return productEntityList
                .stream()
                .map(this::entityToModel)
                .collect(Collectors.toList());
    }

    // Object Mapping Methods
    // MODEL to ENTITY object mapping
    private ProductEntity modelToEntity(ProductDTO productDTO) {
        return ProductEntity.builder()
                .productName(productDTO.getProductName())
                .fabric(productDTO.getFabric())
                .pattern(productDTO.getPattern())
                .description(productDTO.getDescription())
                .stock(productDTO.getStock())
                .size(productDTO.getSize())
                .price(productDTO.getPrice())
                .build();
    }

    // ENTITY to MODEL object mapping
    private ProductDTO entityToModel(ProductEntity productEntity) {
        return ProductDTO.builder()
                .id(productEntity.getId())
                .productName(productEntity.getProductName())
                .fabric(productEntity.getFabric())
                .pattern(productEntity.getPattern())
                .description(productEntity.getDescription())
                .stock(productEntity.getStock())
                .size(productEntity.getSize())
                .price(productEntity.getPrice())
                .childCategoryID(productEntity.getChildCategory().getId())
                .build();
    }
}
