package com.backend_service.ecommerce.Service;

import com.backend_service.ecommerce.Dto.ChildCategoryDTO;
import com.backend_service.ecommerce.Dto.AllCategoryDTO;
import com.backend_service.ecommerce.Entity.ChildCategoryEntity;
import com.backend_service.ecommerce.Entity.SubCategoryEntity;
import com.backend_service.ecommerce.Exceptions.ResourceNotFoundException;
import com.backend_service.ecommerce.Repository.ChildCategoryRepository;
import com.backend_service.ecommerce.Repository.SubCategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ChildCategoryService {

    private final ChildCategoryRepository childCategory_repository;
    private final SubCategoryRepository subCategoryRepository;

    @Autowired
    public ChildCategoryService(ChildCategoryRepository childCategory_repository, SubCategoryRepository subCategoryRepository) {
        this.childCategory_repository = childCategory_repository;
        this.subCategoryRepository = subCategoryRepository;
    }

    // adding child category with sub category id
    public ChildCategoryDTO addChildCategory(Long id, ChildCategoryDTO childCategoryDTO) {
        Optional<SubCategoryEntity> subCategory = subCategoryRepository.findById(id);

        if (subCategory.isPresent()) {

            ChildCategoryEntity childCategoryEntity = modelToEntity(childCategoryDTO);
            childCategoryEntity.setSubCategory(subCategory.get());
            ChildCategoryEntity savedCategory=childCategory_repository.save(childCategoryEntity);
            return entityToModel(savedCategory);
        }else {
            throw new ResourceNotFoundException("Sub category with id :"+id+" not found !");
        }
    }

    // fetching child category by its name or id
    public ChildCategoryDTO getChildCategoryByIdOrName(String query, boolean isDigit) {
        Optional<ChildCategoryEntity> childCategory = isDigit
                ?childCategory_repository.findById(Long.parseLong(query))
                :childCategory_repository.findByName(query);
        if (childCategory.isPresent()){
            return entityToModel(childCategory.get());
        }
        throw new ResourceNotFoundException("Child category with "+query+" not found !");

    }

    // fetching all child categories
    public List<ChildCategoryDTO> getAllChildCategory() {
        List<ChildCategoryEntity> childCategories= childCategory_repository.findAll();

        return childCategories
                .stream()
                .map(this::entityToModel)
                .collect(Collectors.toList());
    }

    // getting all child categories with its sub category and main category details
    public List<AllCategoryDTO> getAllCategories(){
        return childCategory_repository.getAllCategories();
    }


    // Object Mapping Methods
    // MODEL to ENTITY object mapping
    private ChildCategoryEntity modelToEntity(ChildCategoryDTO childCategoryDTO) {
        return ChildCategoryEntity.builder()
                .name(childCategoryDTO.getCategoryName())
                .build();
    }

    // ENTITY to MODEL object mapping
    private ChildCategoryDTO entityToModel(ChildCategoryEntity childCategoryEntity) {
        return ChildCategoryDTO.builder()
                .id(childCategoryEntity.getId())
                .categoryName(childCategoryEntity.getName())
                .subCategoryID(childCategoryEntity.getSubCategory().getId())
                .build();
    }
}
