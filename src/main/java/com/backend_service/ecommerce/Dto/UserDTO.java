package com.backend_service.ecommerce.Dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.bind.DefaultValue;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO {
    private Long id;

    @NotBlank(message = "Name cannot be empty !")
    @Size(max = 50)
    private String name;

    @Email(message = "Please enter a valid email !")
    private String email;

    @NotBlank(message = "Password cannot be empty !")
    @Size(min = 4,max = 10,message = "Password should meet the requirements")
    private String password;

    @NotBlank(message = "Mobile number cannot be empty !")
    @Size(max = 10,message = "Mobile number must be equal to length of 10.")
    private String phone;
}