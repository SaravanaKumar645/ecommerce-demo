package com.backend_service.ecommerce.Dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ChildCategoryDTO {
    private Long id;

    @NotBlank(message = "Child category name cannot be null or empty")
    @Size(max = 40)
    private String categoryName;
    private Long subCategoryID;
}
