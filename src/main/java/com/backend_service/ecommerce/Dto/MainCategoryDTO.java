package com.backend_service.ecommerce.Dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Component
public class MainCategoryDTO {
    private Long mainCategoryId;
    @NotBlank(message = "Main category name cannot be null or empty")
    @Size(max = 40)
    private String mainCategoryName;

    public MainCategoryDTO buildMainCategoryDTO(Long id, String categoryName){
        MainCategoryDTO mainCategoryDTO= new MainCategoryDTO();
        mainCategoryDTO.setMainCategoryId(id);
        mainCategoryDTO.setMainCategoryName(categoryName);
        return mainCategoryDTO;
    }
}
