package com.backend_service.ecommerce.Dto;

import org.springframework.beans.factory.annotation.Value;

public interface AllCategoryDTO {

    @Value("#{target.childCategoryId}")
    Long getChildCategoryId();

    @Value("#{target.childCategoryName}")
    String getChildCategoryName();

    @Value("#{@subCategoryDTO.buildSubCategoryDTO(target.subCategoryId, target.subCategoryName)}")
    SubCategoryDTO getSubCategory();

    @Value("#{@mainCategoryDTO.buildMainCategoryDTO(target.mainCategoryId, target.mainCategoryName)}")
    MainCategoryDTO getMainCategory();
}