package com.backend_service.ecommerce.Dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Component
public class SubCategoryDTO {
    private Long subCategoryId;

    @NotBlank(message = "Sub category name cannot be null or empty")
    @Size(max = 40)
    private String subCategoryName;
    private Long mainCategoryID;


  public SubCategoryDTO buildSubCategoryDTO(Long id, String categoryName){
        SubCategoryDTO subCategoryDTO= new SubCategoryDTO();
        subCategoryDTO.setSubCategoryId(id);
        subCategoryDTO.setSubCategoryName(categoryName);
        return subCategoryDTO;
    }
}
