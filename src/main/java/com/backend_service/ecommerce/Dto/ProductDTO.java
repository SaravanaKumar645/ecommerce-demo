package com.backend_service.ecommerce.Dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductDTO {
    private Long id;

    @NotBlank(message = "Product name cannot be empty !")
    @Size(max = 80)
    private String productName;

    @NotBlank(message = "Fabric cannot be empty !")
    @Size(max = 50)
    private String fabric;

    @NotBlank(message = "Pattern cannot be empty !")
    @Size(max = 50)
    private String pattern;

    @NotBlank(message = "Description cannot be empty !")
    @Size(max = 255)
    private String description;

    @NotNull(message = "Stock cannot be null !")
    private int stock;

    @NotNull(message = "Size cannot be null !")
    private int size;

    @NotNull(message = "Price cannot be null !")
    private int price;

    private Long childCategoryID;
}
