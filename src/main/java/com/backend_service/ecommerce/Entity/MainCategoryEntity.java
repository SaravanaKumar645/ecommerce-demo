package com.backend_service.ecommerce.Entity;

import com.fasterxml.jackson.annotation.*;
import lombok.*;

import javax.persistence.*;
import java.util.Set;
import static javax.persistence.GenerationType.SEQUENCE;

@Entity(name = "MainCategory")
@Table(
        name = "main_category",
        uniqueConstraints = @UniqueConstraint(
                name = "category_name_unique",
                columnNames = "category_name"
        )
)
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MainCategoryEntity {
    @Id
    @SequenceGenerator(
            name = "category_sequence",
            sequenceName = "category_sequence",
            allocationSize = 1,
            initialValue = 10_000
    )
    @GeneratedValue(
            strategy = SEQUENCE,
            generator = "category_sequence"
    )
    @Column(name = "category_id")
    private Long id;

    @Column(name = "category_name",
            nullable = false,
            length = 40
    )
    private String categoryName;

    @OneToMany(
            targetEntity = SubCategoryEntity.class,
            mappedBy = "mainCategory",
            cascade = CascadeType.ALL
    )
    @JsonIgnore
    private Set<SubCategoryEntity> subCategory;

}
