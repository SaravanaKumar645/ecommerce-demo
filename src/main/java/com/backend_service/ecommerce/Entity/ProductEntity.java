package com.backend_service.ecommerce.Entity;

import lombok.*;
import org.hibernate.boot.model.relational.Sequence;

import javax.persistence.*;

import static javax.persistence.GenerationType.SEQUENCE;

@Entity(name = "productList")
@Table(name = "product_list")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProductEntity {
    @Id
    @SequenceGenerator(
            name = "p_seq",
            sequenceName = "p_seq",
            allocationSize = 1,
            initialValue = 40_000
    )
    @GeneratedValue(
            strategy = SEQUENCE,
            generator = "p_seq"
    )
    @Column(name = "product_id")
    private Long id;

    @Column(
            name = "product_name",
            nullable = false,
            length = 80
    )
    private String productName;

    @Column(
            name = "fabric",
            nullable = false,
            length = 50
    )
    private String fabric;

    @Column(
            name = "pattern",
            nullable = false,
            length = 50
    )
    private String pattern;

    @Column(
            name = "description",
            nullable = false
    )
    private String description;

    @Column(
            name = "stock",
            nullable = false
    )
    private int stock;

    @Column(
            name = "size",
            nullable = false
    )
    private int size;

    @Column(
            name = "price",
            nullable = false
    )
    private int price;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(
            name = "child_category_id",
            nullable = false
    )
    private ChildCategoryEntity childCategory;
}
