package com.backend_service.ecommerce.Entity;

import com.fasterxml.jackson.annotation.*;
import lombok.*;

import javax.persistence.*;

import java.util.Set;

import static javax.persistence.GenerationType.SEQUENCE;

@Entity(name = "SubCategory")
@Table(name = "sub_category")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SubCategoryEntity {
    @Id
    @SequenceGenerator(
            name = "subCategory_seq",
            sequenceName = "subCategory_seq",
            allocationSize = 1,
            initialValue = 20_000
    )
    @GeneratedValue(
            strategy = SEQUENCE,
            generator = "subCategory_seq"
    )
    @Column(name = "sub_category_id")
    private Long id;

    @Column(
            name = "sub_category_name",
            nullable = false,
            length = 40
    )
    private String subCategoryName;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(
            name = "category_id",
            nullable = false
    )
    private MainCategoryEntity mainCategory;

    @OneToMany(mappedBy = "subCategory")
    @JsonIgnore
    private Set<ChildCategoryEntity> childCategory;
}
