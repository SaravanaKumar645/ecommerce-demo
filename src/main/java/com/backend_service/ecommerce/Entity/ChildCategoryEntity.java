package com.backend_service.ecommerce.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import javax.persistence.*;
import java.util.List;

import static javax.persistence.GenerationType.SEQUENCE;

@Entity(name = "ChildCategory")
@Table(name = "child_category")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ChildCategoryEntity {
    @Id
    @SequenceGenerator(
            name = "cc_sequence",
            sequenceName = "cc_sequence",
            allocationSize = 1,
            initialValue = 30_000
    )
    @GeneratedValue(
            strategy = SEQUENCE,
            generator = "cc_sequence"
    )
    @Column(name = "child_category_id")
    private Long id;

    @Column(
            name = "child_category_name",
            nullable = false,
            length = 40
    )
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(
            name = "sub_category_id",
            nullable = false
    )
    private SubCategoryEntity subCategory;

    @OneToMany(mappedBy = "childCategory")
    @JsonIgnore
    private List<ProductEntity> productList;
}
