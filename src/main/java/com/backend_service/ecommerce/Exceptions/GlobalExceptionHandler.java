package com.backend_service.ecommerce.Exceptions;

import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
public class GlobalExceptionHandler {
    private final Map<String,Object> apiResponse=new HashMap<>();

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Object> methodArgumentNotValidExceptionHandler(MethodArgumentNotValidException notValidException){
        apiResponse.put("status",HttpStatus.BAD_REQUEST.value());
        apiResponse.put("timestamp",new Date());
        apiResponse.put("message",notValidException
                .getAllErrors()
                .stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
        );
        apiResponse.put("error",true);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(apiResponse);
    }


    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<Object> resourceNotFoundExceptionHandler(ResourceNotFoundException resourceNotFoundException){
         apiResponse.put("status",HttpStatus.NOT_FOUND.value());
         apiResponse.put("timestamp",new Date());
         apiResponse.put("message",resourceNotFoundException.getMessage());
         apiResponse.put("error",true);
         return ResponseEntity.status(HttpStatus.NOT_FOUND).body(apiResponse);
    }

    @ExceptionHandler(DuplicateEntryException.class)
    public ResponseEntity<Object> duplicateEntryExceptionHandler(DuplicateEntryException duplicateEntryException){
        apiResponse.put("status",HttpStatus.BAD_REQUEST.value());
        apiResponse.put("timestamp",new Date());
        apiResponse.put("message",duplicateEntryException.getMessage());
        apiResponse.put("error",true);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(apiResponse);
    }

    @ExceptionHandler(PasswordNotMatchException.class)
    public ResponseEntity<Object> passwordNotMatchExceptionHandler(PasswordNotMatchException passwordNotMatchException){
        apiResponse.put("status",HttpStatus.UNAUTHORIZED.value());
        apiResponse.put("timestamp",new Date());
        apiResponse.put("message",passwordNotMatchException.getMessage());
        apiResponse.put("error",true);
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(apiResponse);
    }

}
