package com.backend_service.ecommerce.Controller;

import com.backend_service.ecommerce.Dto.UserDTO;
import com.backend_service.ecommerce.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@CrossOrigin
@RequestMapping("/api/user")
public class UserController {


    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping(path = "/create-user")
    public ResponseEntity<UserDTO> createUser(@Valid @RequestBody UserDTO userDTO) {
        UserDTO savedUser = userService.registerUser(userDTO);
        return ResponseEntity.status(HttpStatus.CREATED).body(savedUser);
    }

    @PostMapping(path = "/login-user")
    public ResponseEntity<UserDTO> loginUser(@Valid @RequestBody UserDTO userDTO) {
        UserDTO fetchedUser = userService.loginUser(userDTO);
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(fetchedUser);
    }
}
