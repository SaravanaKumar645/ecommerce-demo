package com.backend_service.ecommerce.Controller;

import com.backend_service.ecommerce.Dto.SubCategoryDTO;
import com.backend_service.ecommerce.Service.SubCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(path = "/api/sub_category")
public class SubCategoryController {

    private final SubCategoryService subCategoryService;

    @Autowired
    public SubCategoryController(SubCategoryService subCategoryService) {
        this.subCategoryService = subCategoryService;
    }


    @PostMapping(path = "/{mainCategoryId}/add")
    public ResponseEntity<SubCategoryDTO> addSubCategory(@NotNull @PathVariable(value = "mainCategoryId") Long mainCategoryId,
                                                         @Valid @RequestBody SubCategoryDTO subCategoryDTO){
        SubCategoryDTO savedSubCategory = subCategoryService.addSubCategory(mainCategoryId, subCategoryDTO);
        return ResponseEntity.status(HttpStatus.CREATED).body(savedSubCategory);
    }
    @GetMapping(path = "/get_all")
    public ResponseEntity<List<SubCategoryDTO>> getAllSubCategories(){
        List<SubCategoryDTO> allSubCategories = subCategoryService.getAllSubCategories();
        return ResponseEntity
                .status(HttpStatus.FOUND)
                .body(allSubCategories);
    }

}
