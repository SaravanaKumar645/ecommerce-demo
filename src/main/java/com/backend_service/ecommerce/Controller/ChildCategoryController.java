package com.backend_service.ecommerce.Controller;

import com.backend_service.ecommerce.Dto.ChildCategoryDTO;
import com.backend_service.ecommerce.Dto.AllCategoryDTO;
import com.backend_service.ecommerce.Service.ChildCategoryService;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api/child_category")
public class ChildCategoryController {

    private final ChildCategoryService childCategory_service;

    @Autowired
    public ChildCategoryController(ChildCategoryService childCategory_service) {
        this.childCategory_service = childCategory_service;
    }

    @PostMapping(path = "/{subCategoryID}/add")
    public ResponseEntity<ChildCategoryDTO> addChildCategory(@NotNull @PathVariable(value = "subCategoryID")Long id,
                                                             @Valid @RequestBody ChildCategoryDTO childCategoryDTO){
        ChildCategoryDTO savedChildCategory = childCategory_service.addChildCategory(id, childCategoryDTO);
        return ResponseEntity.status(HttpStatus.CREATED).body(savedChildCategory);
    }

    @GetMapping(path = "/get/{query}")
    public ResponseEntity<ChildCategoryDTO> getChildCategoriesByQuery(@NotBlank @PathVariable(name = "query") String query){

        ChildCategoryDTO childCategory = query.matches("[0-9]+")
                ? childCategory_service.getChildCategoryByIdOrName(query, true)
                : childCategory_service.getChildCategoryByIdOrName(query, false);
        return ResponseEntity.status(HttpStatus.FOUND).body(childCategory);

    }
    @GetMapping(path = "/get_all")
    public ResponseEntity<List<ChildCategoryDTO>> getAllChildCategories(){
        List<ChildCategoryDTO> allChildCategories = childCategory_service.getAllChildCategory();
        return ResponseEntity
                 .status(HttpStatus.FOUND)
                 .body(allChildCategories);
    }

    @GetMapping(path = "/all")
    public ResponseEntity<List<AllCategoryDTO>> getAllCategories(){
        List<AllCategoryDTO> allCategories = childCategory_service.getAllCategories();
        return ResponseEntity.status(HttpStatus.FOUND).body(allCategories);
    }

}
