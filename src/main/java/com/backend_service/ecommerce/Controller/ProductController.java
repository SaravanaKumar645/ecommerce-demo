package com.backend_service.ecommerce.Controller;

import com.backend_service.ecommerce.Dto.ProductDTO;
import com.backend_service.ecommerce.Service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(path = "api/product_list")
public class ProductController {
    private final ProductService product_service;

    @Autowired
    public ProductController(ProductService product_service){
        this.product_service=product_service;
    }

    @PostMapping(path = "/{childCategoryID}/add")
    public ResponseEntity<ProductDTO> addProduct(@NotNull @PathVariable(value = "childCategoryID")Long id,
                                                 @Valid @RequestBody ProductDTO productDTO){
        ProductDTO savedProduct = product_service.addProductList(id, productDTO);
        return ResponseEntity.status(HttpStatus.CREATED).body(savedProduct);
    }

    @GetMapping(path = "/get_by_query/{query}")
    public ResponseEntity<ProductDTO> getProductByQuery(@NotBlank @PathVariable(name = "query") String query){
        ProductDTO fetchedProduct = query.matches("[0-9]+")
                ? product_service.getProductByIdOrName(query, true)
                : product_service.getProductByIdOrName(query, false);
        return ResponseEntity.status(HttpStatus.FOUND).body(fetchedProduct);
    }

    @GetMapping(path = "/get_all")
    public ResponseEntity<List<ProductDTO>> getAllProducts(){
        List<ProductDTO> allProducts = product_service.getAllProduct();
        return  ResponseEntity
                 .status(HttpStatus.FOUND)
                 .body(allProducts);
    }

    @GetMapping(path = "/{childCategoryId}/all")
    public ResponseEntity<List<ProductDTO>> getProductsByChildCategory(@PathVariable(value = "childCategoryId") Long id){
        List<ProductDTO> allProductsByChildCategory = product_service.getAllProductsByChildCategory(id);
        return  ResponseEntity
                .status(HttpStatus.FOUND)
                .body(allProductsByChildCategory);
    }

}
