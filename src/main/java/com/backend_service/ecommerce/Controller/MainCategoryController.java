package com.backend_service.ecommerce.Controller;

import com.backend_service.ecommerce.Dto.MainCategoryDTO;
import com.backend_service.ecommerce.Exceptions.ResourceNotFoundException;
import com.backend_service.ecommerce.Service.MainCategoryService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(path = "/api/main_category")
public class MainCategoryController {

    private final MainCategoryService mainCategoryService;


    public MainCategoryController(MainCategoryService mainCategoryService) {
        this.mainCategoryService = mainCategoryService;
    }

    @PostMapping(path = "/add")
    public ResponseEntity<MainCategoryDTO> addMainCategory(@Valid @RequestBody MainCategoryDTO mainCategoryDTO){
        MainCategoryDTO savedCategory= mainCategoryService.addMainCategory(mainCategoryDTO);
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(savedCategory);
    }
    @GetMapping(path = "/get_all")
    public ResponseEntity<List<MainCategoryDTO>> getAllMainCategories(){
        List<MainCategoryDTO> allMainCategories = mainCategoryService.getAllMainCategories();
        return ResponseEntity
                .status(HttpStatus.FOUND)
                .body(allMainCategories);
    }

    //demo purpose
    @GetMapping(path = "/test")
    public ResponseEntity<Object> getAllCategories(){
       throw new ResourceNotFoundException("No item found with given request !");
    }
}
